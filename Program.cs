﻿using System;
using System.Collections;

namespace exercises
{
    class exercises
    {
        static public void program1()
        {
            // Write a program to print 'Hello' on screen and then print your name on a separate line.
            
            Console.WriteLine("Hello");
            Console.WriteLine("Ravjot Singh Samra");
        }

        static public void program2()
        {
            // Write a program to print the sum of two numbers. 

            int result = 74 + 36;
            Console.WriteLine(result);
        }

        static public void program3()
        {
            // Write a program to divide two numbers and print on the screen.

            int result = 50 / 3;
            Console.WriteLine(result);
        }

        static public void program4()
        {
            // Write a program that takes two numbers as input and display the product of two numbers.

            Console.WriteLine("Input first number:");
            var x = Console.ReadLine();

            Console.WriteLine("Input second number:");
            var y = Console.ReadLine();

            int result = int.Parse(x) * int.Parse(y);
            Console.WriteLine("{0} X {1} = {2}", x, y, result);
        }

        static public void program5()
        {
            // Write a program to print the area and perimeter of a circle. 

            double radius = 7.5;
            double perimeter = 2 * Math.PI * radius;
            double area = Math.PI * Math.Pow(radius, 2);
            
            Console.WriteLine("Perimeter is {0}", perimeter);
            Console.WriteLine("Area is {0}", area);
        }

        static public void program6()
        {
            // Write a program to swap two variables

            string abc = "123";
            string xyz = "456";

            Console.WriteLine("First variable = {0}, Second variable = {1}", abc, xyz);
            
            string swap = abc;
            abc = xyz;
            xyz = swap;
            
            Console.WriteLine("First variable = {0}, Second variable = {1}", abc, xyz);
        }

        static public void program7()
        {
            // Write a program to get a number from the user and print whether it is positive or negative.

            Console.WriteLine("Input number: ");
            double inputtedNumber = double.Parse(Console.ReadLine());

            if (inputtedNumber >= 0)
            {
                Console.WriteLine("Number is positive");
            }
            else 
            {
                Console.WriteLine("Number is negative");
            }
        }

        static public void program8()
        {
            // Take three numbers from the user and print the greatest number. 

            double[] numberArray = new double[3]; // Number of array objects (from 1), index starts at 0

            Console.WriteLine("Input the 1st number:");
            numberArray[0] = double.Parse(Console.ReadLine());

            Console.WriteLine("Input the 2nd number:");
            numberArray[1] = double.Parse(Console.ReadLine());

            Console.WriteLine("Input the 3rd number:");
            numberArray[2] = double.Parse(Console.ReadLine());

            Array.Sort(numberArray);

            Console.WriteLine("The greatest:");
            Console.WriteLine(numberArray[2]);
        }

        static public void program9()
        {
            // Write a one dimensional array of size 5 and populate that with random numbers.

            Random rnd = new Random();
            int[] randomNumbers = new int[5];
            Console.WriteLine("5 random numbers:");

            for(var i = 0; i < randomNumbers.Length; i++)
            {
                randomNumbers[i] = rnd.Next();
                Console.WriteLine(randomNumbers[i]);
            }
        }

        static public void program10()
        {
            // Write a Program that uses stacks to reverse a user inputted string

            Stack reverseInput = new Stack();

            Console.WriteLine("Input anything:");
            string input = Console.ReadLine();

            for(int i = 0; i < input.Length; ++i)
            {
                reverseInput.Push(input[i]);
            }

            Console.WriteLine("Reversed input:");

            foreach (char i in reverseInput)
            {
                Console.Write(i);
            }

            Console.Write('\n');
        }

        static void Main(string[] args)
        {
            Employee empl1 = new Employee(01001001, "John", "123 Fake Street");
            Employee empl2 = new Employee(01000100, "Joe", "99 Pseudo Boulevard");

            while(true)
            {
                Console.WriteLine("\nEnter program number (1 to 10):");
                int choice = int.Parse(Console.ReadLine());
                Console.WriteLine();
                
                switch (choice)
                {
                    case 1:
                        program1();
                        break;
                    case 2:
                        program2();
                        break;
                    case 3:
                        program3();
                        break;
                    case 4:
                        program4();
                        break;
                    case 5:
                        program5();
                        break;
                    case 6:
                        program6();
                        break;
                    case 7:
                        program7();
                        break;
                    case 8:
                        program8();
                        break;
                    case 9:
                        program9();
                        break;
                    case 10:
                        program10();
                        break;
                }
            }
        }
    }

    class Employee
    {
        /* Write a class Employee to have attributes: ID (int), name (string) and address (string). 
           Create two constructors (with and without parameter values). 
           Also, create getter and setter methods for each attributes. 
           Once this class is defined. Create two objects of this class in the main method. */

        public int attr_ID {get; set;}
        public string attr_name {get; set;}
        public string attr_address {get; set;}

        public Employee(int ID, string name, string address)
        {
            attr_ID = ID;
            attr_name = name;
            attr_address = address;
        }

        public Employee()
        {
        }
    }
}
