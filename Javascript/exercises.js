console.log("Test!")

var intArray = [234, 134, 667, 954, 123, 667, 30, 999, 429];
var strArray = ["Dog", "Cat", "Zebra", "Tiger", "Snake", "Goldfish", "Eagle"];

function refreshHTML()
{
    document.getElementById("numericArray").innerHTML = intArray;
    document.getElementById("stringArray").innerHTML = strArray;
}

// Write a Java program to sort a numeric array and a string array.

function sortArray()
{
    // Numerical sort by comparing entire number, not first char. Returns what's passed.
    intArray = intArray.sort((x, y) => x - y);
    // Alphabetical sort is the default.
    strArray = strArray.sort(); 
    refreshHTML();
}

// Write a Java program to remove a specific element from an array.

function removeFromArray()
{
    if (document.getElementById("select_array").value == "number")
    {
        var itemToRemove = document.getElementById("input_value-remove").value;
        var itemIndex = intArray.indexOf(parseInt(itemToRemove));

        if (itemIndex != -1)
        {
            intArray.splice(itemIndex, 1);
        }
    }
    
    else if (document.getElementById("select_array").value == "string")
    {
        var itemToRemove = document.getElementById("input_value-remove").value;
        var itemIndex = strArray.indexOf(itemToRemove);
        
        if (itemIndex != -1)
        {
            strArray.splice(itemIndex, 1);
        }
    }
    
    refreshHTML();
}

// Write a Java program to find the maximum and minimum value of an array.

function minMaxValuesArray()
{
    if (intArray.length > 0)
    {
        document.getElementById("numericArrayMin").innerHTML = Math.min.apply(Math, intArray);
        document.getElementById("numericArrayMax").innerHTML = Math.max.apply(Math, intArray);
    }
    else
    {
        document.getElementById("numericArrayMin").innerHTML = "--";
        document.getElementById("numericArrayMax").innerHTML = "--";
    }
}

// Write a Java program to reverse an array of integer values.

function reverseIntegerArray()
{
    intArray = intArray.sort((x, y) => y - x)
    refreshHTML();
}